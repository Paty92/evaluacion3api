package root.bdmascotas;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.bdmascotas.dao.MascotasJpaController;
import root.bdmascotas.dao.exceptions.NonexistentEntityException;
import root.bdmascotas.entity.Mascotas;

/**
 *
 * @author patiw
 */
@Path("mascota")
public class mascotas {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarmascotas() {

      /*  Mascotas mc = new Mascotas();
        mc.setId("01");
        mc.setNombre("marco");
        mc.setEdad("2");
        mc.setRaza("pug");
        mc.setTipomascota("perro");

        Mascotas mc2 = new Mascotas();
        mc2.setId("02");
        mc2.setNombre("romeo");
        mc2.setEdad("4");
        mc2.setRaza("pelocorto");
        mc2.setTipomascota("gato");
    
     List<Mascotas> lista=new ArrayList<Mascotas>();
     
    lista.add(mc);
    lista.add(mc2);*/
    
    MascotasJpaController dao= new MascotasJpaController();
    List<Mascotas> Lista=dao.findMascotasEntities();
      
      
return Response.ok(200).entity(Lista).build();

    }
    
    
    
    @POST
    @Produces(MediaType.APPLICATION_JSON) 
    public Response add(Mascotas mascotas){
    
        
        try{
         MascotasJpaController dao= new MascotasJpaController();
         dao.create(mascotas);
        }catch(Exception ex){
        Logger.getLogger(Mascotas.class.getName()).log(Level.SEVERE, null, ex);

             }
        
           return Response.ok(200).entity(mascotas).build();

    }
    
    
    @DELETE
      @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON) 
    public Response delete(@PathParam("iddelete") String iddelete){
    
          try{
         MascotasJpaController dao= new MascotasJpaController();
         dao.destroy(iddelete);
           } catch (NonexistentEntityException ex) {
            Logger.getLogger(Mascotas.class.getName()).log(Level.SEVERE, null, ex);
  }
      
        return Response.ok("mascota eliminada").build();
        
    }
    
    @PUT
      public Response update(Mascotas mascotas) throws Exception{

          try{
          MascotasJpaController dao= new MascotasJpaController();
          dao.edit(mascotas);
          } catch (NonexistentEntityException ex) {
            Logger.getLogger(Mascotas.class.getName()).log(Level.SEVERE, null, ex);
      
          }
     return Response.ok(200).entity(mascotas).build();
    
}
}